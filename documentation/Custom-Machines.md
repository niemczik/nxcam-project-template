# Create and use Custom Machines

Basics [Learning Advantage -> NX/12.0/CAM/NC Simulation and verfication/Constructing and testing a kinematic model](https://training.plm.automation.siemens.com/lms/ssla/player.htm?courseId=nx12-esvx_define_kinematic_simulation-en_US-esvb&courseDirectory=/lms/stub_jca&studentName=Niemczik,Lars&studentId=843AA7C3E685EBC17ECAAB1A8FE74BD3&contentServer=https://du8t0r5o6uqcx.cloudfront.net&courseURL=/lib/standard/nx/12/esvx_define_kinematic_simulation/en_US/index.html?v=3D79CE81A18A057CDB4F3093F6D8F4B2)

|Learning Advantage selfregistration|
|-|
|<img src="Media/Learning_Advatage_-_HowTo_Self_registration.png" height="300">|

## Customize a Machine (for CSE)

1. Copy an existing machine (OOTB-Machine, out of the box machine, other maybe found on Posthub) form the "%UGII_BASE_DIR%/MACH/resources/library/machine/installed_machines/" folder to "%NXCAM-SIMULATION-REPOSITORY%/cam_custom/library/machine/installed_machines" or whereever you want to manage your custom machines.
    - For this project we use simliar conventions, so the machine will be available through the "UGII_CAM_LIBRARY_INSTALLED_MACHINES_NXCAMSIMULATION_DIR" enviroment variable in NX
2. Add the machine to Machine Database "%UGII_BASE_DIR%/MACH/resources/library/machine/ascii/machine_database.dat" or [wherever your machine database is located](Using-a-custom-machine-Library).
    1. Take a look at the notes in the header of the file!
    2. Consinder copying and modifing an existing entry
    3. Assign an individual name
    4. Adjust description etc. if necessary
    5. Adjust path to .dat (-> aka. machine definition) to make sure it points to the corret locations!
         - For this project Make use of the "UGII_CAM_LIBRARY_INSTALLED_MACHINES_NXCAMSIMULATION_DIR" environment variable.
3. Edit the %Machine_Name%_%Controller_Name%.dat, e.g. "sim08_mill_5ax_simumerik.dat", Files and redirect the Paths to the Custom Machine
    - For this project, make use of the "UGII_CAM_LIBRARY_INSTALLED_MACHINES_NXCAMSIMULATION_DIR" environment variable
4. Check if the Machine is Loading Succesfull in NX and the correct Machine Configuration File (MCF) is loaded.
    - For this project start NX through the [Batchscript](./startnx.bat), so the environment is setuped correctly).
    - Start the Machine Configurator in NX and check the paths.
5. For customiztaion and creation of a custom Machine see the tutorial in [Learning Advantage](https://training.plm.automation.siemens.com/mytraining).
6. Happy Modding

### Create remotable machines
sim01_mill_3ax_custom implements a generic (Standard DIN 66025/G-Code) 3-axis machine, which is extended for remoting capabilities (and some more). For creating a remotable machine use this as the template for the controller and replace the kinematic model with these of the machine to create (follow the standard way). To remove and add axis edit the AXISADDRESS attribute of the Lex file (More axis maybe require some additional work, take a look on other example machines for this).

### Create and use a custom machine Library

For projetcs like this here it is desirable to seperate the project specific machine library from the general one. This is possible through the following steps.

   1. Copy the complete content of "%UGII_BASE_DIR%/MACH/resources/library/machine/ascii/" to the desired location.
        - For this project these could be found in "%REPO%/cam_custom/resources/library/machine/ascii/"
   2. Make use of the "UGII_CAM_LIBRARY_MACHINE_ASCII_DIR" and "UGII_CAM_LIBRARY_MACHINE_DATA_DIR" variables and route them to the new location.
        - Both have to point to the same location otherwise the TCL script/ database init will fail (No logical combination to sperate the script from the database was successful).
   3. Clean the new database from unnecessary content

