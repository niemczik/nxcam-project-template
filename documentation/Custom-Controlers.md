# Develop & customize Controlers

This covers the two development approaches for CSE and especially the way how to debug CSE.

## Customizing Controllers

There are currently two/three ways of creatign customized Controllers the old XML, the new python based way and the way between. Pure XML based is considered obsolet (and not really recommended anymore, because it's error-prone). So only the last two are discussed. The first is required for Sinumerik, Fanuc and Heidehein. The other targets the Generic controllers.

A machine configuration file (MCF) is the root, which is accessed by NX (or other CSE simulation enviroments) to get further Information especially the Controller and how it's implmented. Simumeric etc. also include controller configuration files (CCF) which are shared between machines with the same controller. Attention CSE relies heavily on naming conventions so beware of them. An introducation to deeper customization could be found [here](https://community.sw.siemens.com/s/article/NX-CAM-CSE-Customization-with-Python-April-2020) and [especially here](https://community.sw.siemens.com/servlet/fileField?entityId=ka54O000000PJqtQAG&field=Attachment__Body__s).

## The way between CCFs+MCF+Python

In this approach CCFs+MCF contain a lot of different definitions related to standard variables, methods, metacodes etc. An introduction to the Details could be found [here](https://community.sw.siemens.com/s/article/customize-your-machine-tool-simulation-advanced-30-videos).

## The (mostly) Python only way

The sim01 generic OOTB Example does not use any CCF, it just contains a minimal MCF, mostly everything is implemented in Python (Variables, Methods, Metacode, etc.).

## Debugging CSE

All/Most well known IDEs for Pytho (Pycharm, VS Code, VS, Ecclipse) are using the [Pydevd Debugger](https://github.com/fabioz/PyDev.Debugger) at the end, in different Versions. But the actual usage depends slightly on the IDE.

### Debugging with PyCharm (Professional)

This is harder then it might look on first sight. First a matchhing Version of PyCharm Professional (remote Debugging capabilty is required) is needed, which supports a working version of Pydevd debugger:

|NX Version|PyCharm (Pydevd)|
|:---------|:---------------|
|NX 1899   |2019.2.6 (1.4.0)|

But Pydevd prior 1.7.0 comes with a nasty little bug, which causes an fatal exception in NX while resetting a machine. Fortunaly there is an easy solution (the exception is caused by wrongly referenced modules). The fixed version can be found [here](./scripts/patches/).

Another tip, instead of calling Pydevd through the PyCharm-Pydevd interface call Pydevd directly (PyCharm-Pydevd just imports Pydevd).

``` python
import pydevd
pydevd.settrace('localhost', port=5678, stdoutToServer=True, stderrToServer=True)
```

For CAM-Simulations it's important to disconnect the debugger during reset to prevent fatal exceptions. For this put the following to the "Finalize" method of the controller to debug. This Method is called while reseting or leaving the simulation:

```` python
def Finalize(self):
    #...
    if pydevd.connected:
        pydevd.stoptrace()
````

While other Controllers, especially the Sinumerik controller, are seem run pobably even with an attaced debugger (couple of hours without a crash), the generic sim01 Exmaple required the replacement of the NX Python distribution to fix an further issue.

### Replacing the NX Python distribution

NX comes with an own Python distribution, for NX Open this could be replaced by a local installation through some environment variables (see ugii_env_ug.dat in your NX installation folder). But CSE will allways use the NX distributed Python (feature or bug?), which seems to cause some trouble especially for the Standard/Generic sim01 machine (an error in the socket, see following nx log excerpt, while debugging/reseting the machine).

```` text
Loaded module c:\program files\siemens\nx1899\nxbin\python\_socket.pyd 7ffd4d840000 15000 ee0f0d34-45d81f80-384b7b96-54b40b1a-1 version = 3.7.4150.1013
+++ Invalid write to 000001A78F065A00
    rsp:       a070ffd188, rip:      1a78f065a00, rbp:                0
    rsi:      1a794b4c0e8, rdi:      1a794b4c0e0
    rax:      1a78f065a00, rbx:      1a790fdd9c0, rcx:      1a79454cd58, rdx:      1a790fe7b58
    r8:                3c, r9:       1a790f20000, r10:      1a79a3d83e0, r11:               48
    r12:       a070ffd3b8, r13:                2, r14:          4efe0a0, r15:                6
````

Replacing it with the latest, bugfixed, version seems to solve this (it runs as smooth as the others). Just download the latest embeddable version of python supported by NX, for NX 1899 this is Python 3.7.x (at time of writing this is [3.7.7](https://www.python.org/downloads/release/python-377/)). Make sure to backup the NX distribution of Python before replacing it!

Sometimes NX/CSE will crash even with the replacement of the Python distribution (it doesn't seem to be common, at least these crashes are mostly multiple hours apart), but for these crashes no simple solution seem to be available, because these typically refer to native libraries.

