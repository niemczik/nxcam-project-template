# Content

This is a summary of the lessons learned through creating custom machines with (totally) custom controllers.

1. [Create & use custom Machines](Custom-Machines.md)
2. [Develop & customize Controlers](Custom-Controlers.md)
