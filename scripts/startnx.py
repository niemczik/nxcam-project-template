import os, sys, argparse
from dotenv import load_dotenv
import subprocess

try:
    parser = argparse.ArgumentParser(description='Starting NX in a specfict configuration')
    parser.add_argument('--nxroot', type=str, nargs='?', default=os.getenv('UGII_BASE_DIR'), help='NX version to start. In none is given the system default wide NX Version is used (spcified by UGII_BASE_DIR)')
    parser.add_argument('--config', type=str, nargs='+', default=[], help='Files with the configurating environment variables.')

    args = parser.parse_args()

    ugii_base_dir = args.nxroot
    nx_exe = f'{ugii_base_dir}/nxbin/ugraf.exe'
    print(f'Starting NX from {nx_exe}')

    root = os.path.dirname(__file__)
    nxcam_simulation_root = os.path.abspath(os.path.join(root, ".."))

    os.environ["NXCAM_SIMULATION_ROOT"] = nxcam_simulation_root
    os.environ["UGII_BASE_DIR"] = ugii_base_dir

    for config in args.config:
        load_dotenv(os.path.join(root, config))

    environ = list(filter(lambda s: s.startswith("UGII"), os.environ.keys()))
    environ += list(filter(lambda s: s.startswith("NXCAM_SIMULATION"), os.environ.keys()))
    environ += list(filter(lambda s: s.startswith("CAMSIM"), os.environ.keys()))
    print("Starting NX with following Enviroment custom variables:")
    for envvar in environ:
        value = os.path.expandvars(os.environ[envvar])
        os.environ[envvar] = value
        print("{0} = {1}".format(envvar, value))
    print("Starting NX from {0}...".format(args.nxroot))
    subprocess.run(nx_exe)
except Exception:
    e = sys.exc_info()[0]
    print(e)