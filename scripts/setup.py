import os
import subprocess
import shutil

root = os.path.dirname(__file__)

# Virtual env
venv = os.path.join(root,"..",".venv")
try:
    print("Try to create virtual enviroment")
    subprocess.run("virtualenv " + venv)
except:
    print("virtualenv not found")
    print("Install virtualenv")
    subprocess.run("py -3.7 -m pip install virtualenv")
    print("Create virtualenv")
    subprocess.run("py -3.7 -m virtualenv " + venv)

# Install requirments
venv_scripts = os.path.join(venv, "Scripts")
venv_pip = os.path.join(venv_scripts, "pip")
requiremnts = os.path.join(root, "requirements.txt")
print("Install requirements")
subprocess.run(venv_pip + " install -r " + requiremnts)

# Apply patch
patch_name = "pydevd_trace_dispatch.py"
patch = os.path.join(root, "patches", patch_name)
patch_destination = os.path.join(venv, "Lib", "site-packages", "_pydevd_bundle", patch_name)
try:
    shutil.copy(patch, patch_destination)
except Exception as e:
    print(e)
    print("Could not write access. Apply the following Patch manual:")
    print("cp '{0}' '{1}' -force".format(patch, patch_destination))