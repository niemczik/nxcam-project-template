# NXCAM-Project-Template

Following content will only treat project related stuff. General explenations for general technical backgrounds like custom Machines and Controllers, Debugging are shown in the Wiki.

## Modding a Machinen Controller

Prerequirements (for debugging machine controllers):

   1. Python ([NX Python compability recommendation](https://docs.plm.automation.siemens.com/tdoc/nx/1899/release_notes#uid:index_xid920453:xid920496:xid385122)), take the latest patch/bugfix version should also work ([mostly even better](#-Replacing-the-NX-Python-distribution)).

Recomendations (for development):

   1. NX Machine Configurator AddOn
   2. Visual Studio (for Simulation Service, NX Open)
   3. PyCharm 2019 (for CSE customization)
   4. Visual Studio Code (for the rest of the stuff, especialy documentation)

### Setup the Python

The virtual python eviroment should be automatically initialized through runnning [the setup script](./scripts/setup.py). The virtual enviroment is primary used to manage module dependecies. It is likely that the Python distribution provided by NX must be replaced, take a look into "Wiki->Custom Controllers->Replacing the NX Python distribution" for details.

### Starting customized NX

Whan you start NX through the ["startnx.bat"](./startnx.bat) Batchfile, it will set all necessary [enviroment variabes](./scripts/nx.env) to use the virtual Python enviroment, the library with the custom machines and will start the "Remoting Server" so the Simualtion Service can control the NX session.

### Customization & Dev

Take a look into the [docs](./documentation/home.md) for further informations about CAM customization and hints for development.
