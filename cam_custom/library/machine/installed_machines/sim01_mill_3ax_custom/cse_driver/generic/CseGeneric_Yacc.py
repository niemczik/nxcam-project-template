# 
# Copyright (c) 2018 Siemens Industry Software Inc. 
# Unpublished - All rights reserved 
#
# 
# 
# ================================================================

import sys
import os
import ply.yacc as yacc
import ply.lex as lex
import CseGeneric_Lex


from CSEWrapper import NCBasicType
from CSEWrapper import NCExpressionFactory
from CSEWrapper import CallFactory
from CSEWrapper import NCLValue
from CSEWrapper import CseParseError

fileLog = open(os.path.split(os.path.realpath(__file__))[0] + r'\log.txt','a')
logger = lex.PlyLogger(fileLog)

class ExprCore():

    # Get the token map
    tokens = CseGeneric_Lex.tokens
    
    precedence = ()
       
       
    def __init__(self, debug = 0, callF = None, exprSys = None):
        self.debug = debug
        self.names = { }
        modname = self.__class__.__name__
        self.debugfile = modname + ".dbg"
        self.tabmodule = modname + "_" + "parsetab"
        self.parser = yacc.yacc(module=self, debug=self.debug, debugfile=self.debugfile, optimize=0, write_tables=0, tabmodule=self.tabmodule, errorlog = logger)

        # init nc zeilen parsing 
        if callF != None:
            self.SetCallFactory(callF)
         
        # init expression parsen (NC variablen fenster)
        if exprSys != None:
            self.SetExprSystem(exprSys)          
    
    def SetCallFactory(self, callF):
        self.callFactory = callF
        self.exprSystem = self.callFactory.GetExprSystem()
        self.valueFactory = self.exprSystem.GetValueFactory()
        self.exprFactory = self.exprSystem.GetExprFactory()

    def SetExprSystem(self, exprSys):
        self.exprSystem = exprSys
        self.valueFactory = self.exprSystem.GetValueFactory()
        self.exprFactory = self.exprSystem.GetExprFactory() 
    
    def p_expression_1(self, p):
        '''expression : FLOAT_VALUE'''
        p[0] = self.CreateLiteralExprFromDouble(p[1])

    def p_expression_2(self, p):
        '''expression : INTEGER_VALUE'''
        p[0] = self.CreateLiteralExprFromInteger(p[1])
    
    def p_error(self, p):
        self.error = ""
    
    def CreateLiteralExprFromBool(self, p):
        return self.exprFactory.CreateLiteral(self.valueFactory.CreateBoolValue(p))
    
    def CreateLiteralExprFromInteger(self, p):
        return self.exprFactory.CreateLiteral(self.valueFactory.CreateIntegerValue(p))

    def CreateLiteralExprFromDouble(self, p):
        return self.exprFactory.CreateLiteral(self.valueFactory.CreateDoubleValue(p))

    def CreateLiteralExprFromString(self, p):
        return self.exprFactory.CreateLiteral(self.valueFactory.CreateStringValue(p))
    
    
class Parser(ExprCore):
    start = 'line'
    
    def p_line_1(self, p):
        '''line : lineContent'''
        
    def p_line_2(self, p):
        '''line : lineContent EOL'''
    
    def p_lineContent(self, p):
        '''lineContent : linePrefix numberedLine optionalComment''' 
    
    def p_linePrefix_1(self, p):
        '''linePrefix : 'N' INTEGER_VALUE'''    
    
    def p_linePrefix_2(self, p):
        '''linePrefix :  '''    
         
    def p_numberedLine_1(self, p):
        '''numberedLine : '''

    def p_numberedLine_2(self, p):
        '''numberedLine : address numberedLine'''
           
    def p_optionalComment_1(self, p):
        '''optionalComment : '''

    def p_optionalComment_2(self, p):
        '''optionalComment : COMMENT optionalComment''' 
    
    def p_address_1(self, p):
        '''address : AXISADDRESS intfloatValue'''
        
        dictArgsNC = {}
        dictArgsNC["Value"] = p[2]
        self.callFactory.CreateMetacodeCall(p[1], dictArgsNC)
           
    def p_address_2(self, p):
        '''address : AXISADDRESS EQUALS intfloatValue'''
        
        dictArgsNC = {}
        dictArgsNC["Value"] = p[3]
        self.callFactory.CreateMetacodeCall(p[1], dictArgsNC)
        
    def p_address_3(self, p):
        '''address : FLOATADDRESS intfloatValue'''
        
        dictArgsNC = {}
        dictArgsNC["Value"] = p[2]
        self.callFactory.CreateMetacodeCall(p[1], dictArgsNC)
        
    def p_address_4(self, p):
        '''address : PARAMETERADDRESS intfloatValue'''
        
        dictArgsNC = {}
        dictArgsNC["Value"] = p[2]
        self.callFactory.CreateMetacodeCall(p[1], dictArgsNC)
        
    def p_address_5(self, p):
        '''address : 'G' INTEGER_VALUE'''
        self.callFactory.CreateMetacodeCall("G" + str(p[2]))
        
    def p_address_6(self, p):
        '''address : 'M' INTEGER_VALUE'''
        self.callFactory.CreateMetacodeCall("M" + str(p[2]))

    def p_intfloatValue_1(self, p):
        '''intfloatValue : INTEGER_VALUE'''
        p[0] = self.CreateLiteralExprFromInteger(p[1])

    def p_intfloatValue_2(self, p):
        '''intfloatValue : MINUS INTEGER_VALUE'''
        p[0] = self.CreateLiteralExprFromInteger(-p[2])

    def p_intfloatValue_3(self, p):
        '''intfloatValue : PLUS INTEGER_VALUE'''
        p[0] = self.CreateLiteralExprFromInteger(p[2])

    def p_intfloatValue_4(self, p):
        '''intfloatValue : FLOAT_VALUE'''
        p[0] = self.CreateLiteralExprFromDouble(p[1])

    def p_intfloatValue_5(self, p):
        '''intfloatValue : MINUS FLOAT_VALUE'''
        p[0] = self.CreateLiteralExprFromDouble(-p[2])

    def p_intfloatValue_6(self, p):
        '''intfloatValue : PLUS FLOAT_VALUE'''
        p[0] = self.CreateLiteralExprFromDouble(p[2])

    def parse(self, s):
        self.error = None
        self.g_bCallMacro = False
        self.parser.parse(s, tokenfunc=get_token)
        return self.error == None
            
last_token = None

def get_token():
    # global last_token
    last_token = CseGeneric_Lex.lex.token()
    return last_token

class ParserExpr(ExprCore):

    g_pExpr = None
    
    start = 'startExpr'

    def p_startExpr(self, p):
        'startExpr : expression'
        ParserExpr.g_pExpr = p[1]

    def parse(self, s):
        self.error = None
        self.g_bCallMacro = False
        self.parser.parse(s)
        if self.error != None:
            return None
        
        return ParserExpr.g_pExpr    
        g_pExpr = None
    

    
    
    
