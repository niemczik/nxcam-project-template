# 
# Copyright (c) 2018 Siemens Industry Software Inc. 
# Unpublished - All rights reserved 
#
# 
# 
# ================================================================


import sys
import math
from CSEWrapper import ChannelState
from CSEWrapper import NCStruct
from CSEWrapper import NCBasicType
from CSEWrapper import Vector3
from CSEWrapper import Matrix4


class BasicMethods: 
    def ReadMachineKinematic(channel, strChainName : str):
        pExprSystem = channel.GetExprSystem()
        
        # This method reads current active chain information from kinematic model
        # This method returns a struct with field names: 
        # "XAxis"[string], "YAxis"[string],"ZAxis"[string] ,"ToolRotarySoftLimitMin"[double],"ToolRotarySoftLimitMax"[double], "PartRotarySoftLimitMin"[double], "PartRotarySoftLimitMax"[double]   
        # "MachineType" [string], "PartRotaryAxis"[string], "ToolRotaryAxis"[string], "ToolNormalVector" [vector]
        
        # define struct fields
        fields = {}
        fields["strXAxis"] = pExprSystem.CreateBasicType(NCBasicType.STRING)
        fields["strYAxis"] = pExprSystem.CreateBasicType(NCBasicType.STRING)
        fields["strZAxis"] = pExprSystem.CreateBasicType(NCBasicType.STRING)
        fields["dToolRotarySoftLimitMin"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["dToolRotarySoftLimitMax"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["dPartRotarySoftLimitMin"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["dPartRotarySoftLimitMax"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["dSpindleRotarySoftLimitMin"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["dSpindleRotarySoftLimitMax"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["strMachineType"] = pExprSystem.CreateBasicType(NCBasicType.STRING)
        fields["strPartRotaryAxis"] = pExprSystem.CreateBasicType(NCBasicType.STRING)
        fields["strToolRotaryAxis"] = pExprSystem.CreateBasicType(NCBasicType.STRING)
        fields["strSpindleRotaryAxis"] = pExprSystem.CreateBasicType(NCBasicType.STRING)
        fields["vecPartRotaryVector"] = pExprSystem.CreateBasicType(NCBasicType.OBJECT)
        fields["vecToolRotaryVector"] = pExprSystem.CreateBasicType(NCBasicType.OBJECT)
        fields["vecSpindleRotaryVector"] = pExprSystem.CreateBasicType(NCBasicType.OBJECT)
        
        pStructType = pExprSystem.CreateStructType(fields)
        pStruct = pStructType.CreateStruct()
        
        strSwivelingChainName = channel.GetStateAttribute("GV_strSwivelingChainName")
                
        # query kinematic type from current active chain     
        channel.SetStructField(pStruct, "strMachineType", channel.GetKinematicChainType(strSwivelingChainName))

        # query part/tool rotary axis name from current active chain 
        strPartRotaryAxis = channel.GetKinematicChainAxis(strSwivelingChainName, "partrotaryaxis")
        strToolRotaryAxis = channel.GetKinematicChainAxis(strSwivelingChainName, "toolrotaryaxis")

        nAxisListSize = len(channel.GetKinematicChainAxisList(strSwivelingChainName))
        strSpindleAxis = channel.GetKinematicChainAxisList(strSwivelingChainName)[nAxisListSize-1]
        
        # query axis names from current active chain
        channel.SetStructField(pStruct, "strXAxis", channel.GetKinematicChainAxis(strSwivelingChainName, "xaxis"))
        channel.SetStructField(pStruct, "strYAxis", channel.GetKinematicChainAxis(strSwivelingChainName, "yaxis"))
        channel.SetStructField(pStruct, "strZAxis", channel.GetKinematicChainAxis(strSwivelingChainName, "zaxis"))

        channel.SetStructField(pStruct, "strPartRotaryAxis", strPartRotaryAxis)
        channel.SetStructField(pStruct, "strToolRotaryAxis", strToolRotaryAxis)

        channel.SetStructField(pStruct, "strSpindleRotaryAxis", strSpindleAxis)

        if strSpindleAxis != "":
            channel.SetStructField(pStruct, "dSpindleRotarySoftLimitMin", channel.GetJointParameter(strSpindleAxis, "SoftLimitMin"))
            channel.SetStructField(pStruct, "dSpindleRotarySoftLimitMax", channel.GetJointParameter(strSpindleAxis, "SoftLimitMax"))
            
            # query spindle rotary direction vector
            vecToolLVector = channel.GetKinematicChainPos(strSwivelingChainName,channel.CreateCoordinate(),False, "tool").GetRowVec(2)
            channel.SetStructField(pStruct, "vecSpindleRotaryVector", vecToolLVector)
       
        if strToolRotaryAxis != "":
            channel.SetStructField(pStruct, "dToolRotarySoftLimitMin", channel.GetJointParameter(strToolRotaryAxis, "SoftLimitMin"))
            channel.SetStructField(pStruct, "dToolRotarySoftLimitMax", channel.GetJointParameter(strToolRotaryAxis, "SoftLimitMax"))
            
            # query tool rotary axis vector 
            vecToolRotaryAxis = channel.GetKinematicChainPos(strSwivelingChainName,channel.CreateCoordinate(),False, "axis", strToolRotaryAxis).GetRowVec(2)
            channel.SetStructField(pStruct, "vecToolRotaryVector", vecToolRotaryAxis)
             
        if strPartRotaryAxis != "":
            channel.SetStructField(pStruct, "dPartRotarySoftLimitMin", channel.GetJointParameter(strPartRotaryAxis, "SoftLimitMin"))
            channel.SetStructField(pStruct, "dPartRotarySoftLimitMax", channel.GetJointParameter(strPartRotaryAxis, "SoftLimitMax"))
            
            # query part rotary axis vector 
            vecPartRotaryAxis = channel.GetKinematicChainPos(strSwivelingChainName,channel.CreateCoordinate(),False, "axis", strPartRotaryAxis).GetRowVec(2)
            channel.SetStructField(pStruct, "vecPartRotaryVector", vecPartRotaryAxis)

        # for internal diagnostics
        channel.Assert(not(channel.GetStateAttribute("GV_bActivateDebug")), "Warning", "ReadMachineKinematic ok")
        
        return pStruct

    # Set value to given linear joint
    def SetJointValueLinear(channel, strJointName, dValue, strFlag):
        channel.SetTargetJointValue(strJointName, dValue, strFlag)

    # Set value to given rotational joint
    def SetJointValueRotational(channel, strJointName, dValue, strFlag):
        channel.SetJointRotationMode("normal", strJointName)
        channel.SetTargetJointValue(strJointName, dValue)
    
    # Set value to given inf rotational joint
    def SetJointValueInfRotational(channel, strJointName, dValue, strFlag):
        if channel.GetJointParameter(strJointName, "type") == "spindle":
            channel.SetSpindleSpeed(strJointName, 0.0)
            channel.SetSpindleMode(strJointName, "AXIS")
        
        if (channel.GetJointParameter(strJointName, "softlimitmax") - channel.GetJointParameter(strJointName, "softlimitmin")) < 360.0:
            channel.SetJointRotationMode("normal", strJointName)
        else:
            channel.SetJointRotationMode("shortcut", strJointName)
        
        channel.SetTargetJointValue(strJointName, dValue)
    
    # Set linear offset values to given transformation
    def SetModalTransformationOffset(channel, strTrafoName : str, dX : float, dY : float, dZ : float):
        matTrafo = Matrix4()
        matTrafo.Translate(Vector3(dX, dY, dZ))
        
        channel.ActivateTransformation(strTrafoName, False, False)
        channel.ResetTransformation(strTrafoName)
        channel.SetTransformationMatrix(strTrafoName, matTrafo)
        channel.ActivateTransformation(strTrafoName, True, True)

        return None

    ################################################################################
    # Use CalculateTurningAngles method to calculate angles for table, tool and spindle axis 
    ################################################################################
    def CalculateTurningAngles(channel : ChannelState, dBeta : float , dGamma : float, matInitTurningCsys : Matrix4):
        pExprSystem = channel.GetExprSystem()
        # define struct fields
        fields = {}
        fields["dToolAngle1"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["dToolAngle2"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["dPartAngle1"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["dPartAngle2"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["dSpindleAngle1"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["dSpindleAngle2"] = pExprSystem.CreateBasicType(NCBasicType.DOUBLE)
        fields["strPartRotaryAxis"] = pExprSystem.CreateBasicType(NCBasicType.STRING)
        fields["strToolRotaryAxis"] = pExprSystem.CreateBasicType(NCBasicType.STRING)       
        pStructType = pExprSystem.CreateStructType(fields)
        pStruct = pStructType.CreateStruct()

        strSwivelingChainName = channel.GetStateAttribute("GV_strSwivelingChainName")      
        vecTarget = Vector3(0, 0, 1)
        matRotation = Matrix4()
        matRotation.RotateByAngles(0,math.radians(dBeta), 0, "RPY")
        matRotation *= matInitTurningCsys # matInitTurningCsys can contain additional rotation
        vecTarget = matRotation.MultiplyVec(vecTarget)

        #structAngles contains IKS calculated part and tool angle pairs
        structAngles = channel.CalculateIKSAngles(strSwivelingChainName, vecTarget)
        dToolAngle1 = channel.GetStructField(structAngles, "dToolAngle1")
        dToolAngle2 = channel.GetStructField(structAngles, "dToolAngle2")
        dPartAngle1 = channel.GetStructField(structAngles, "dPartAngle1")
        dPartAngle2 = channel.GetStructField(structAngles, "dPartAngle2")
                
        #GMe_SwivelingCalculateSpindleAngles calculates spindle orientation for the current working plane
        dSpindleAngle1 = channel.CallMethod("GMe_SwivelingCalculateSpindleAngles", dGamma, dPartAngle1, dToolAngle1, matRotation)
        dSpindleAngle2 = channel.CallMethod("GMe_SwivelingCalculateSpindleAngles", dGamma, dPartAngle2, dToolAngle2, matRotation)

        channel.SetStructField(pStruct, "dToolAngle1", dToolAngle1)
        channel.SetStructField(pStruct, "dToolAngle2", dToolAngle2)
        channel.SetStructField(pStruct, "dPartAngle1", dPartAngle1)
        channel.SetStructField(pStruct, "dPartAngle2", dPartAngle2)
        channel.SetStructField(pStruct, "dSpindleAngle1", dSpindleAngle1)
        channel.SetStructField(pStruct, "dSpindleAngle2", dSpindleAngle2)
        channel.SetStructField(pStruct, "strToolRotaryAxis", channel.GetStructField(structAngles, "strToolRotaryAxis"))
        channel.SetStructField(pStruct, "strPartRotaryAxis", channel.GetStructField(structAngles, "strPartRotaryAxis"))

        return  pStruct
    
    def SetToolLengthCorrection(channel : ChannelState, 
                                bReset : bool, 
                                bActive : bool, 
                                bModal : bool, 
                                strTrafoName : str, 
                                bRelative : bool, 
                                vecToolLength: Vector3 = None
                                ):
              
        if vecToolLength == None:
            dToolLengthX = channel.GetZCorrection()
            dToolLengthY = channel.GetQCorrection()
            dToolLengthZ = channel.GetLCorrection()
        else: 
            dToolLengthX = vecToolLength[0]
            dToolLengthY = vecToolLength[1]
            dToolLengthZ = vecToolLength[2]


        strSwivelingChainName = channel.GetStateAttribute("GV_strSwivelingChainName")
        structKinematic = BasicMethods.ReadMachineKinematic(channel, strSwivelingChainName)

        channel.Assert(channel.DoesTransformationExist(strTrafoName), "FATAL", "Given Transformation does not exist! Check transformation stack in CCF/MCF.")

        if channel.IsTransformationActive(strTrafoName):
            channel.ActivateTransformation(strTrafoName, False, bModal)
        
        if bReset:
            channel.ResetTransformation(strTrafoName)

        if bActive:
            strXAxis = channel.GetStructField(structKinematic, "strXAxis")
            strYAxis = channel.GetStructField(structKinematic, "strYAxis")
            strZAxis = channel.GetStructField(structKinematic, "strZAxis")
            
            if strXAxis != None:
                channel.Assert(channel.HasJoint(strXAxis), "FATAL", "Axis name "+strXAxis +" does not exist in current channel! Check axis settings.")
                channel.SetTransformationOffset(strTrafoName, strXAxis, dToolLengthX, bRelative)
               
            if strYAxis != None:
                channel.Assert(channel.HasJoint(strYAxis), "FATAL", "Axis name "+strYAxis +" does not exist in current channel! Check axis settings.")
                channel.SetTransformationOffset(strTrafoName, strYAxis, dToolLengthY, bRelative)
            
            if strZAxis != None:
                channel.Assert(channel.HasJoint(strZAxis), "FATAL", "Axis name "+strZAxis +" does not exist in current channel! Check axis settings.")
                channel.SetTransformationOffset(strTrafoName, strZAxis, dToolLengthZ, bRelative)
        
        channel.ActivateTransformation(strTrafoName, True, bModal)    
        
        return None
