﻿# 
# Copyright (c) 2018 Siemens Industry Software Inc. 
# Unpublished - All rights reserved 
#
# 
# 
# ================================================================


import sys
import os
import math
import inspect


if "UGII_CAM_AUXILIARY_DIR" in os.environ:
    sys.path.append(os.environ['UGII_CAM_AUXILIARY_DIR'] + r'\cse')
elif "UGII_CAM_BASE_DIR" in os.environ:
    sys.path.append(os.environ['UGII_CAM_BASE_DIR']+r'\auxiliary\cse')
elif "UGII_BASE_DIR" in os.environ:
    sys.path.append(os.environ['UGII_BASE_DIR']+r'\mach\auxiliary\cse')

import CSEWrapper
from CSEWrapper import NCBasicType
from CSEWrapper import VariableManager
import CseGeneric_Yacc
from CseGeneric_Metacodes import ControllerMetacodes
from CseGeneric_Methods import ControllerMethods


class Controller:
    def __init__(self):
        self.m_NCParser = CseGeneric_Yacc.Parser()
        self.m_ExprParser =  CseGeneric_Yacc.ParserExpr()
        self.m_ctrlCommand = ControllerCommand()

    def ParseNCLine(self, strLine, callFactory):
        self.m_NCParser.SetCallFactory(callFactory)
        return self.m_NCParser.parse(strLine)

    def ParseNCExpression(self, strLine, exprsys):
        self.m_ExprParser.SetExprSystem(exprsys)
        return self.m_ExprParser.parse(strLine)

    def ExecuteCommand(self, strCommandName, listArgsNC, state):
        return False

    def HasMetacode(self, strMetacodeName : str) -> bool:
        return hasattr(ControllerMetacodes, strMetacodeName)

    def HasMCParameter(self, strMetacodeName : str, strMCParamName : str) -> bool:
        try:
            metacode = getattr(ControllerMetacodes, strMetacodeName)
        except AttributeError:
            return False
        sig = inspect.signature(metacode)
        if strMCParamName in sig.parameters:
            return sig.parameters[strMCParamName].annotation == "MCParam"
        return False

    def ExecuteMetacode(self, strMetacodeName, dictArgs, listArgs, state):
        try:
            metacode = getattr(ControllerMetacodes, strMetacodeName)
        except AttributeError:
            return None

        dictArgsNative = {}
        for strName, arg in dictArgs.items():
            dictArgsNative[strName] = arg.GetNativeValue()

        listArgsNative = []
        for arg in listArgs:
            listArgsNative.append(arg.GetNativeValue())

        if listArgsNative != []:
            dictArgsNative["listArgs"] = listArgsNative

        return metacode(state, **dictArgsNative)

    def HasMethod(self, strMethodName):
        return hasattr(ControllerMethods, strMethodName)

    def GetMethodType(self, strMethodName, exprSystem):
        try:
            metacode = getattr(ControllerMethods, strMethodName)
        except AttributeError:
            return None

        sig = inspect.signature(metacode)

        if sig.return_annotation == Signature.empty:
            return None
        elif sig.return_annotation == bool:
            return exprSystem.CreateBasicType(NCBasicType.BOOL)
        elif sig.return_annotation == int:
            return exprSystem.CreateBasicType(NCBasicType.INTEGER)
        elif sig.return_annotation == float:
            return exprSystem.CreateBasicType(NCBasicType.DOUBLE)
        elif sig.return_annotation == str:
            return exprSystem.CreateBasicType(NCBasicType.STRING)

        return None

    def ExecuteMethod(self, strMethodName, listArgsNC, exprSystem):
        try:
            method = getattr(ControllerMethods, strMethodName)
        except AttributeError:
            return None

        listArgsNative = []
        for arg in listArgsNC:
            listArgsNative.append(arg.GetNativeValue())

        result = method(exprSystem.GetChannelState(), *listArgsNative)
        
        return exprSystem.GetValueFactory().CreateValue(result)

    def InitializeChannel(self, state):
        newCtrlState = ControllerState()
        newCtrlState.InitializeChannel(state)
        return newCtrlState

    def CloneChannel(self, channelobj, state):
        return channelobj.Clone(state)

class ControllerCommand:
    pass

class ControllerState:
    def __init__(self):
        pass

    def CloneChannel(self, channelobj, state):
        return channelobj.Clone(state)

    def InitializeChannel(self, state):
        exprSystem = state.GetExprSystem()
        valFactory = exprSystem.GetValueFactory()
        varManager = exprSystem.GetVarManager()
        varManager.SetVarAccessMode(VariableManager.GLOBAL_ONLY)
        varManager.ActivateAutoDeclare(True)
        varManager.AllowMultiDeclare(True)
        state.SetFullCircleMode(True)

        return ControllerState()


def CreateController():
        return CSEWrapper.Controller(Controller())

if __name__ == '__main__':

    parser = CreateController();
    while 1:
        try:
            s = input('GCode > ')
        except EOFError:
            break
        if not s: continue
        parser.ParseNCLine(s, None)
