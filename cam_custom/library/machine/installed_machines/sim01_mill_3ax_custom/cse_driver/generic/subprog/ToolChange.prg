﻿

##LANGUAGE AC
  INT nToolID;
  STRING strCarrierName;
  
  nToolID = GetSubProgParameter(0);
  strCarrierName = "S";

  IF (nToolID > 0);
    generateTool (getToolNameByNumber(nToolID), strCarrierName);
  ENDIF;

  IF (exist (getCurrentTool (strCarrierName)));
    visibility (     getCurrentTool (strCarrierName), OFF, TRUE);
    collision  (OFF, getCurrentTool (strCarrierName));
    release    (     getCurrentTool (strCarrierName));
  ENDIF;

  IF (exist (getNextTool (strCarrierName)));
    grasp      (    getNextTool (strCarrierName), getSpindleObject (strCarrierName));
    position   (    getNextTool (strCarrierName), 0, 0, 0, 0, 0, 0);
    visibility (    getNextTool (strCarrierName), ON, TRUE);
    collision  (ON, getNextTool (strCarrierName), 2, -0.01);
    activateNextTool (strCarrierName);
  ENDIF;
##LANGUAGE NATIVE
M17

