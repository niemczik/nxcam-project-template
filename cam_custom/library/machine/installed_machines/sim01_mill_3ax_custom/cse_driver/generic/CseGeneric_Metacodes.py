﻿# 
# Copyright (c) 2018 Siemens Industry Software Inc. 
# Unpublished - All rights reserved 
#
# 
# 
# ================================================================

from CSEWrapper import ChannelState
from CSEBasic import BasicMethods
from CseGeneric_Methods import ControllerMethods
from CseGeneric_Variables import Internal_Variables
from CSEWrapper import Vector3
from CSEWrapper import Matrix4

class ControllerMetacodes: 
    # Activate cutting edge data set of tool
    def D(channel, Value):
        channel.SetToolCorrection(Value, channel.GetMainSpindle())
        strTransformatioName = "$TOOL"
        if Value != 0:
            BasicMethods.SetModalTransformationOffset(channel,
                                                      strTransformatioName,
                                                      channel.GetZCorrection(), #X-Offset
                                                      channel.GetQCorrection(), #Y-Offset
                                                      channel.GetLCorrection()  #Z-Offset
                                                      )
        else:
            channel.ActivateTransformation(strTransformatioName, False, True)
        return True

    # Feed rate value
    def F(channel, Value):
        channel.SetFeed(Value)
        Internal_Variables.dFeedvalue = Value
        return True

    # Interpolation parameter to state the circle center point coordinates in the directions X
    def I(channel, Value):
        channel.SetCircleParameter("I", Value)
        return True
    
    # Interpolation parameter to state the circle center point coordinates in the directions Y
    def J(channel, Value):
        channel.SetCircleParameter("J", Value)
        return True
    
    # Interpolation parameter to state the circle center point coordinates in the directions Z
    def K(channel, Value):
        channel.SetCircleParameter("K", Value)
        return True

    # Spindle speed in rpm for the master spindle
    def S(channel, Value):
        channel.SetSpindleSpeed(channel.GetMainSpindle(), Value)
        Internal_Variables.dSpindleSpeed = Value
        return True

    # Preselect given tool number and call tool change subprogram ToolChange.prg
    def T(channel, Value):
        Internal_Variables.nPreselectedToolNumber = Value
        return True

    # Cartesian coordinate of the target position in the X direction
    def X(channel, Value):
        BasicMethods.SetJointValueLinear(channel, channel.GetFirstGeoAxisName(), Value, "default")
        return True

    # Cartesian coordinate of the target position in the Y direction
    def Y(channel, Value):
        BasicMethods.SetJointValueLinear(channel, channel.GetSecondGeoAxisName(), Value, "default")
        return True

    # Cartesian coordinate of the target position in the Z direction
    def Z(channel, Value):
        BasicMethods.SetJointValueLinear(channel, channel.GetThirdGeoAxisName(), Value, "default")
        return True

    # Command for the activation of rapid traverse motion
    def G0(channel):
        channel.SetMotionType("linearrapid")
        return True
    
    # Command for the activation of linear interpolation
    def G1(channel):
        channel.SetMotionType("linear")
        return True
    
    # Command for the activation of clockwise circular interpolation
    def G2(channel):
        channel.SetMotionType("CircularRight")
        channel.SetIJKPositioningMode("Relative")
        return True
    
    # Command for the activation of counter clockwise circular interpolation
    def G3(channel):
        channel.SetMotionType("CircularLeft")
        channel.SetIJKPositioningMode("Relative")
        return True

    # Working plane X/Y Infeed direction Z, plane selection 1st - 2nd geometry axis
    def G17(channel):
        channel.SetWorkingPlane("XY")
        return True

    # Working plane Z/X Infeed direction Y, plane selection 3rd - 1st geometry axis
    def G18(channel):
        channel.SetWorkingPlane("ZX")
        return True

    # Working plane Y/Z Infeed direction X, plane selection 2nd - 3rd geometry axis
    def G19(channel):
        channel.SetWorkingPlane("YZ")
        return True
    
    # Deactivate tool radius correction
    def G40(channel):
        channel.SetToolRadiusCorrectionMode(False)
        return True
    
    # Activate tool radius correction with machining direction left of the contour
    def G41(channel):
        channel.SetToolRadiusCorrectionMode(True, True)
        return True
    
    # Activate tool radius correction with machining direction right of the contour
    def G42(channel):
        channel.SetToolRadiusCorrectionMode(True, False)
        return True

    # Non-modal suppression of all programmable and settable frames
    def G53(channel):
        channel.ActivateTransformation("FIXED", False, False)
        channel.ActivateTransformation("ROTATIONAL", False, False)
        return True

    # Call of the 1st settable zero offset
    def G54(channel):
        ControllerMethods.ActivateLocalOffset(channel, 1, "FIXED")
        return True

    # Call of the 2nd settable zero offset
    def G55(channel):
        ControllerMethods.ActivateLocalOffset(channel, 2, "FIXED")
        return True
 
    # Call of the 3rd settable zero offset
    def G56(channel):
        ControllerMethods.ActivateLocalOffset(channel, 3, "FIXED")
        return True
    
    # Call of the 4th settable zero offset
    def G57(channel):
        ControllerMethods.ActivateLocalOffset(channel, 4, "FIXED")
        return True
    
    # Call of the 5th settable zero offset
    def G58(channel):
        ControllerMethods.ActivateLocalOffset(channel, 5, "FIXED")
        return True
    
    # Call of the 5th settable zero offset
    def G59(channel, X : "MCParam" = None, Y : "MCParam" = None, Z : "MCParam" = None):
        strTrafoName = "FIXED"
        matOffset = Matrix4()
        
        matOffset.Translate(Vector3(X, Y, Z))
        matOffset *= channel.GetTransformationMatrix(strTrafoName)
        
        channel.ActivateTransformation(strTrafoName, False, False)
        channel.SetTransformationMatrix(strTrafoName, matOffset)
        return True

    # Command for the activation of modal absolute dimensions
    def G90(channel):
        channel.SetPositioningMode("Absolute")
        return True

    #Command for the activation of modal relative dimensions
    def G91(channel):
        channel.SetPositioningMode("Relative")
        return True

    # Programmed stop
    def M0(channel):
        channel.SuspendProgramExecution(False)
        return True

    # Optional stop
    def M1(channel):
        channel.SuspendProgramExecution(True)
        return True
    
    # End of program, main program (as M30)
    def M2(channel):
        channel.SetGlobalSpindleSpeed(0.0)
        channel.SetEndSubprog()
        return True
    
    # Spindle clockwise
    def M3(channel):
        strSpindleName = channel.GetMainSpindle()
        channel.SetSpindleMode(strSpindleName, "CW")
        return True
    
    # Spindle counter clockwise
    def M4(channel):
        strSpindleName = channel.GetMainSpindle()
        channel.SetSpindleMode(strSpindleName, "CCW")
        return True
    
    # Spindle stop
    def M5(channel):
        strSpindleName = channel.GetMainSpindle()
        channel.SetSpindleMode(strSpindleName, "NStop")
        channel.SetSpindleSpeed(strSpindleName, Internal_Variables.dSpindleSpeed)
        return True
    
    # Tool change (default setting)
    def M6(channel):
        channel.CallSubprog("ToolChange", True)
        channel.AddSubprogParameter(Internal_Variables.nPreselectedToolNumber, True)
        Internal_Variables.nActiveToolNumber = Internal_Variables.nPreselectedToolNumber
        return True
    
    # Coolant ON
    def M8(channel):
        channel.SetStateAttribute("SANX_strCoolant", "ON")
        return True    
    
    # Coolant OFF
    def M9(channel):
        channel.SetStateAttribute("SANX_strCoolant", "OFF")
        return True    
    
    # End of subprogram
    def M17(channel):
        channel.SetEndSubprog()
        return True
    
    # End of program, main program (as M2)
    def M30(channel):
        channel.SetGlobalSpindleSpeed(0.0)
        channel.SetEndSubprog()
        return True
