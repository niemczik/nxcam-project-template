# 
# Copyright (c) 2018 Siemens Industry Software Inc. 
# Unpublished - All rights reserved 
#
# 
# 
# ================================================================

import sys
import math
from CSEWrapper import ChannelState
from CSEWrapper import NCStruct
from CSEWrapper import NCValue

class Internal_Variables:
    # this class includes global variables without relation to controller variables
    # Note: variables in Internal_Variables calls are globally defined regardless to the channel 
    dSpindleSpeed = 0
    nPreselectedToolNumber = 0 
    nActiveToolNumber = 0
    dFeedValue = 0
    
    nLocalOffsetID = 0
    dLocalOffsetX = [0, 0, 0, 0, 0, 0]
    dLocalOffsetY = [0, 0, 0, 0, 0, 0]
    dLocalOffsetZ = [0, 0, 0, 0, 0, 0]




