# 
# Copyright (c) 2018 Siemens Industry Software Inc. 
# Unpublished - All rights reserved 
#
# 
# 
# ================================================================

import sys
import re
import os
import ply.lex as lex

from CSEWrapper import CseParseError

tokens = [
    "SI_ID",
    "AXISADDRESS", 
    "PARAMETERADDRESS", 
    "FLOATADDRESS", 
    "COMMENT", 
    "EQUALS", 
    "ANYCOMMAND", 
    "FLOAT_VALUE", 
    "INTEGER_VALUE", 
    "EOL", 
    "PLUS", 
    "MINUS", 
    "TIMES", 
    "DIVIDE",
    "AND_OP",
    "OR_OP", 
    "NOT_OP",
    "IF_OP",
    "ELSE_OP",
    "ENDIF_OP"
    ]  

literals = [
    'M',
    'N',
    'G', 
    '=', 
    '[', 
    ']'
    ]

t_EQUALS  = r'='
t_ignore = " \t"
t_PLUS    = r'\+'
t_MINUS   = r'-'
t_TIMES   = r'\*'
t_DIVIDE  = r'/'

def t_FLOAT_VALUE_1(t):
    r'\d+\.\d*'
    t.value = float(t.value)
    t.type = "FLOAT_VALUE"
    return t

def t_FLOAT_VALUE_2(t):
    r'\.\d+'
    t.value = float(t.value)
    t.type = "FLOAT_VALUE"
    return t

def t_INTEGER_VALUE(t):
    r'\d+'
    t.value = int(t.value)
    return t
    
def t_COMMENT_1(t):
    r'\([^\)]*\)'
    t.type = "COMMENT"
    return t
    
def t_FLOATADDRESS_1(t):
    r'D'
    t.type = "FLOATADDRESS"
    return t
    
def t_FLOATADDRESS_2(t):
    r'F'
    t.type = "FLOATADDRESS"
    return t

def t_FLOATADDRESS_6(t):
    r'S'
    t.type = "FLOATADDRESS"
    return t
        
def t_FLOATADDRESS_7(t):
    r'T'
    t.type = "FLOATADDRESS"
    return t
    
def t_PARAMETERADDRESS_1(t):
    r'I'
    t.type = "PARAMETERADDRESS"
    return t

def t_PARAMETERADDRESS_2(t):
    r'J'
    t.type = "PARAMETERADDRESS"
    return t

def t_PARAMETERADDRESS_3(t):
    r'K'
    t.type = "PARAMETERADDRESS"
    return t

def t_AXISADDRESS_1(t):
    r'A'
    t.type = "AXISADDRESS"
    return t

def t_AXISADDRESS_2(t):
    r'B'
    t.type = 'AXISADDRESS'
    return t

def t_AXISADDRESS_3(t):
    r'C'
    t.type = "AXISADDRESS"
    return t

def t_AXISADDRESS_4(t):
    r'U'
    t.type = "AXISADDRESS"
    return t

def t_AXISADDRESS_5(t):
    r'V'
    t.type = "AXISADDRESS"
    return t

def t_AXISADDRESS_6(t):
    r'W'
    t.type = "AXISADDRESS"
    return t

def t_AXISADDRESS_7(t):
    r'X'
    t.type = "AXISADDRESS"
    return t

def t_AXISADDRESS_8(t):
    r'Y'
    t.type = "AXISADDRESS"
    return t

def t_AXISADDRESS_9(t):
    r'Z'
    t.type = "AXISADDRESS"
    return t
        
def t_EOL(t):
    r'\n|\r'
    return t

def t_error(t):
    raise CseParseError

lex = lex.lex(optimize=0, debug=0, errorlog = lex.NullLogger())
