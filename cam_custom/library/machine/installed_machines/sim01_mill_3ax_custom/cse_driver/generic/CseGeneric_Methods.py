﻿# 
# Copyright (c) 2018 Siemens Industry Software Inc. 
# Unpublished - All rights reserved 
#
# 
# 
# ================================================================

import sys
import math
from CSEWrapper import ChannelState
from CSEWrapper import NCStruct
from CSEWrapper import Vector3
from CSEWrapper import Matrix4
from CSEBasic import BasicMethods
from CseGeneric_Variables import Internal_Variables

class ControllerMethods(BasicMethods):
    
    # This internal method is called by NX CAM ISV if a settable zero offset is activated. 
    # It is called for all joints with the corresponding offset value. 
    # It is called before setOffsetSysVar to apply additional calculated offsets. 
    # This default implementation supports X, Y, Z axis
    # For all other axes this implementation must be overwritten in the MCF
    # strJointName: name of the joint
    
    def calculateJointOffsetFromPosition(channel, 
                                         strJointName : str,
                                         dX : float, 
                                         dY : float, 
                                         dZ : float, 
                                         dA : float, 
                                         dB : float, 
                                         dC : float):
        if channel.GetFirstGeoAxisName() == strJointName:
            return dX
        elif channel.GetSecondGeoAxisName() == strJointName:
            return dY
        elif channel.GetThirdGeoAxisName() == strJointName:
            return dZ
        else:
            return None

    # This internal method is called by NX CAM ISV if a settable zero offset is activated. 
    # It is called for all joints with the corresponding offset value.
    # This default implementation supports X, Y, Z, A, B, C axis
    
    # strOffsetID: number of the settable zero offset that should be activated
    # strJointName: name of the joint
    # dOffsetValue: offset for this joint
    # dPresetValue: ?
   
    def setOffsetSysVar(channel,
                        strOffsetID : str,
                        strJointName : str,
                        dOffsetValue : float,
                        dPresetValue : float):
                
        iIdx = int(strOffsetID)
        Internal_Variables.nLocalOffsetID = iIdx

        if channel.GetFirstGeoAxisName() == strJointName:
            Internal_Variables.dLocalOffsetX[iIdx] = dOffsetValue
        elif channel.GetSecondGeoAxisName() == strJointName:
            Internal_Variables.dLocalOffsetY[iIdx] = dOffsetValue
        elif channel.GetThirdGeoAxisName() == strJointName:
            Internal_Variables.dLocalOffsetZ[iIdx] = dOffsetValue
        else:
            return None
        
        return True

    def CalculateCenterFromRadius(channel : ChannelState,
                                  dExactFirstValue : float,
                                  dExactSecondValue : float,
                                  dProgrammedFirstValue : float,
                                  dProgrammedSecondValue : float,
                                  dSignedRadius : float,
                                  bClockwise : bool,
                                  struct : NCStruct):
        dRadius = abs(dSignedRadius)

        dDiffFirstValue = dProgrammedFirstValue - dExactFirstValue
        dDiffSecondValue = dProgrammedSecondValue - dExactSecondValue

        dDiffLength = math.sqrt(dDiffFirstValue * dDiffFirstValue + dDiffSecondValue * dDiffSecondValue)
        dHalfDist = dDiffLength / 2

        dPrecision = channel.GetMotionPrecision()

        if dPrecision <= 0.0:
            dPrecision = 0.001

        if dRadius + dPrecision < dHalfDist:
            return False
        elif dRadius < dHalfDist:
            dRadius = dHalfDist

        if (bClockwise and (dSignedRadius < 0.0)) or ((not bClockwise) and (dSignedRadius > 0.0)):
            dOffsetFirstValue = -dDiffSecondValue / dDiffLength
            dOffsetSecondValue = dDiffFirstValue / dDiffLength
        else:
            dOffsetFirstValue = dDiffSecondValue / dDiffLength
            dOffsetSecondValue = -dDiffFirstValue / dDiffLength

        dCenterOffset = math.sqrt(dRadius * dRadius - dHalfDist * dHalfDist)
        dCenterFirstValue = dExactFirstValue + dDiffFirstValue / 2 + dCenterOffset * dOffsetFirstValue
        dCenterSecondValue = dExactSecondValue + dDiffSecondValue / 2 + dCenterOffset * dOffsetSecondValue

        channel.SetStructField(struct, 'CenterX', dCenterFirstValue)
        channel.SetStructField(struct, 'CenterY', dCenterSecondValue)

        return None

    def IsSkipLevelSupported(channel : ChannelState, nLevel : int) -> bool:
        return nLevel >= 1 and nLevel <= 9

    # Activates Local Offset on given transformation and offset ID
    def ActivateLocalOffset(channel : ChannelState, nOffsetID : int, strTrafoName : str):
        channel.LoadOffset(str(nOffsetID))
        BasicMethods.SetModalTransformationOffset(channel,
                                                  strTrafoName, 
                                                  Internal_Variables.dLocalOffsetX[nOffsetID], 
                                                  Internal_Variables.dLocalOffsetY[nOffsetID], 
                                                  Internal_Variables.dLocalOffsetZ[nOffsetID]
                                                  )
        return True

    # Executes every machine cycle (default every 0.1 sec)
    # To remove this method edit the corresponding machine configuration (.MCF)
    def TEventHandler(channel: ChannelState):
        pass

