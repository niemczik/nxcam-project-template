# 
# Copyright (c) 2019 Siemens Industry Software Inc. 
# Unpublished - All rights reserved 
#
# 
# 
# ================================================================

import sys
import os
import json

def ReadKeywordDict(keywords, out, tokens):
    for token, item in keywords.items():
        if isinstance(item, list):
            tokens.append(token)
            for keyword in item:
                out[keyword] = token
        elif isinstance(item, dict):
            ReadKeywordDict(item, out, tokens)

def ReadRegexDict(regex, out):
    for token, item in regex.items():
        if isinstance(item, list):
            if len(item) == 1:
                out[token] = item[0]
            else:
                for expr in item:
                    out.setdefault(token, []).append(expr)
        elif isinstance(item, dict):
            ReadRegexDict(item, out)

def ReadTokenConfigFile(strControllerName, reserved, regexDict, tokens):
    dir = os.path.dirname(os.path.abspath(__file__))

    with open(dir + '\\' + strControllerName + r'_Tokens.json', 'r', encoding='utf-8') as jsonFile:
        syntaxMap = json.load(jsonFile)

    keywords = syntaxMap.get('keywords')
    if keywords != None:
        ReadKeywordDict(keywords, reserved, tokens)

    regex = syntaxMap.get('regex')
    if regex != None:
        ReadRegexDict(regex, regexDict)
