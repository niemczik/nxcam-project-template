###############################################################################
# dbc_machine_ascii.tcl - DBC Event Handler for database stored as ascii file
###############################################################################
##############################################################################
# REVISIONS
#   Date       Who              Reason
#   04-Dec-98  binu &         Initial
#              naveen
#   24-Mar-99  Dave Yoon      Remove source paths
#   11-Jun-99  Joachim Meyer  New format of the data file
#                             (For description see .dat file)
#   17-Jun-99  Joachim Meyer  Remove test file open
#   05-Jul-99  Joachim Meyer  Use MOM_abort for error message
#                             Use MOM__boot for sourcing general tcl files
#                             Dummy procedure MOM__halt
#   28-Jul-99  Joachim Meyer  Adapt Function ASC_load_data_file
#                             to modified interface 
#   13-Oct-99  Joachim Meyer  Add DBC_ask_library_values
#   15-Feb-01  Joachim Meyer  Set dbc_mct_part_name for the
#                             machine model partfile name
#   29-Jan-02  Joachim Meyer  Evaluate environment variable UGII_CAM_LIBRARY_MACHINE_DATA_DIR
#                             instead of UGII_CAM_LIBRARY_MACHINE_ASCII_DIR in order to
#                             locate the machine_database.dat file
#   10-Dec-02  Joachim Meyer  Add env var UGII_CAM_LIBRARY_MACHINE_CONFIG_DIR to the file spec of
#                             the machine configuration file if no path information 
#                             is specified in the library
#   15-Jan-2004 rlm           Add ASC_output_head and support for rigidity
#   30-Jan-2004 rlm           Move ASC_write to dbc_ascii_general
#   12-Feb-2004 rlm           Add library revision information
#   25-jun-2004 JM            Introduce dbc_machine_name variable and set it to libref
#   26-Jul-2005 JM            Set machine name to description
#   20-Mar-2007 JM            Improve retrieval of the machine config file specification,
#                             better error handling
#   14-Feb-2008 Thomas Schulz Add GRAPHICS Attribute for NX sample have different 
#                             controllers for the same machine tool model
#   14-Mar-2008 JM            Append .prt to the machine part name in the default case <libref>.prt
#   24-Sep-2008 rlm           Use previous format record to build new one
#   29-May-2015 Paul Wilbrandt Add mct_cotroller and mct_type to ask in DBC_retrieve 
##############################################################################
#

proc ASC_m_create_filename {env_var_name filename} \
{
#
# Creates a complete filename by evaluating an environment variable
# for the directory information
#
    set env_var [MOM_ask_env_var $env_var_name]
    if { $env_var == "" } \
    {
         set message "Can't read environment variable $env_var_name"
         MOM_abort "\n $message"
    }

    set fname ""
    set fname [append fname $env_var $filename]

    return $fname

}

proc MOM__boot {} \
{



# source some general procedures


#
# dbc_ascii_general.tcl
#
  set filename \
      [ASC_m_create_filename "UGII_UG_LIBRARY_DIR" "dbc_ascii_general.tcl"]
  if { [catch {source $filename}] == "1" } \
  {
      set message "Can't load .tcl file: $filename"
      MOM_abort "\n $message"
  }


###    source /cam/v160/jm_mct/dbc_ascii_general.tcl

}

proc MOM__halt {} \
{

}




#---------------------------------------------
proc DBC_init_db {} \
{

global asc_debug 
global asc_file_name
global dbc_part_units
global asc_units

#
# Global variables set by DBC for Input/Output
#

global dbc_lhs_exp
global dbc_rhs_exp
global dbc_relop
global dbc_query
global dbc_subqry1
global dbc_subqry2
global dbc_boolop 
global dbc_class_name
global dbc_attr_count
global dbc_attr_id 
global dbc_query_count
global dbc_libref 
global dbc_var_list
global dbc_rididity
global dbc_machine_database_file_name


set dbc_lhs_exp     ""
set dbc_rhs_exp     ""
set dbc_relop   ""
set dbc_query   ""
set dbc_subqry1 ""
set dbc_subqry2 ""
set dbc_boolop  ""
set dbc_class_name  ""
set dbc_attr_count  0
set dbc_attr_id     ""
set dbc_query_count 0
set dbc_libref ""
set dbc_var_list ""
set dbc_rigidity 1.0


set asc_debug 0

# Create the filename for the machine_database.dat

   set env_var UGII_CAM_LIBRARY_MACHINE_DATA_DIR
   set asc_file_name [ASC_m_create_filename "$env_var" "machine_database.dat"]
   set dbc_machine_database_file_name $asc_file_name
   
#  And load the file into memory

   set ret_val \
      [ASC_load_data_file $asc_file_name $asc_units(unknown) 0]
   if { $ret_val == "1" } \
   {
      set message "Open error for UG/Library ASCII Data File: $asc_file_name"
      set message "$message \n $asc_file_name"
      MOM_abort "\n $message"
   }


}


#---------------------------------------------
proc DBC_retrieve {} {
#---------------------------------------------
#
# global input
# ------------
   global asc_debug

   global dbc_libref
   global dbc_retrieve_var_list
   global dbc_rigidity


#
# global output
# -------------

#
# Path and filname of the MOM_post config file for this machine
       global dbc_mom_post_config_file_name

#
# type, controller, manufacturer and description
       global dbc_mct_type
       set dbc_mct_type mct_unknown
       global dbc_mct_controller
       set dbc_mct_controller mct_unknown
       global dbc_mct_manufacturer
       set dbc_mct_manufacturer mct_unknown
       global dbc_mct_description
       set dbc_mct_description mct_unknown

# might be overwritten later by description
       global dbc_machine_name
       set dbc_machine_name $dbc_libref


#
# partfile name for machine graphics
#
       global dbc_mct_part_name

   if { $asc_debug == "1" } \
   {
        puts " =========================================="
        puts " procedure  DBC_retrieve for machine"
        puts " libref -> $dbc_libref"
   }

#
# Look for the desired libref
#
   set found [ASC_array_search_machine $dbc_libref db_row]

   if { $found == 2} \
   {
       set dbc_rigidity 1.0
       return
   }

   # TS: use GRAPHICS attribute if available
   # use libref if not available
   set dbc_mct_part_name [ASC_ask_att_val GRAPHICS $db_row "" unknown flag]
   if {$flag == 0} \
   { 
        set dbc_mct_part_name ""
        append dbc_mct_part_name $dbc_libref ".prt"
   } \
   else \
   {
      # check whether suffix prt is missing - if so add it.
      if {[string first ".prt" $dbc_mct_part_name] < 0} \
         { append dbc_mct_part_name ".prt" }
   }







#
# Ask the necessary attribute values 
#
  set dbc_mct_type \
      [ASC_ask_att_val T $db_row "" unknown flag]
      
  set dbc_mct_controller \
      [ASC_ask_att_val CNTR $db_row "" unknown flag]
      
  set dbc_mct_manufacturer \
      [ASC_ask_att_val MNF $db_row "" unknown flag]

  set dbc_mct_description \
      [ASC_ask_att_val DESCR $db_row "" unknown flag]

#  
   if { [string compare $dbc_mct_description "unknown"] != 0 } \
   {
       set dbc_machine_name $dbc_mct_description
   }



  set dbc_rigidity \
      [ASC_ask_att_val RIGID $db_row "%g" unknown flag]

#
#  Configuration file name for list of post/MTD
#
   set db_file_name \
       [ASC_ask_att_val POST $db_row "" "" flag]



   set dbc_mom_post_config_file_name [create_config_file_name $db_file_name]


   if { $dbc_mom_post_config_file_name == "" } \
   {
       if { $db_file_name == "" } \
       {
           set message_1 " \n Cannot find configuration file (.dat) for the machine."
       } \
       else \
       {
           set message_1 " \n Cannot find configuration file ($db_file_name) for the machine."
       }       
       set message_2  " \n Check the environment variable settings for  "
       set message_3  " \n UGII_CAM_LIBRARY_INSTALLED_MACHINES_DIR or UGII_CAM_LIBRARY_MACHINE_CONFIG_DIR"
       set message_4  " \n The system tries to locate the file using either "
       set message_5  " \n \$\{UGII_CAM_LIBRARY_INSTALLED_MACHINES_DIR\}<libref> or"
       set message_6  " \n \$\{UGII_CAM_LIBRARY_MACHINE_CONFIG_DIR\}. "
       set message_7  " \n You can specify the name of the file in the DB attribute POST or"
       set message_8  " \n the system uses <libref>.dat."


       set message ""
       append message $message_1   \
                      $message_2   \
                      $message_3   \
                      $message_4   \
                      $message_5   \
                      $message_6   \
                      $message_7   \
                      $message_8   

       MOM_abort $message

   }
}



proc create_config_file_name { db_file_name } \
{
#
#  
#
# global input
   global dbc_libref

   if { [string first "\\"    $db_file_name] >= 0    || \
        [string first "/"     $db_file_name] >= 0    || \
        [string first "\$\{"  $db_file_name] >= 0}      \
   {
#
#       path information in file specification  --> take it as is

        return $db_file_name
   }

#
#  create the path information, new and old
   set file_name_ret [create_file_name_new $db_file_name]
   if { $file_name_ret == "" } \
   {
        set file_name_ret [create_file_name_old $db_file_name]
   }

   return $file_name_ret
}


proc create_file_name_new { db_file_name } \
{
#
#  procedure creates a complete file name where the machine config .dat file
#  is located using the new MACH machine management structure which is based 
#  on the env setting UGII_CAM_LIBRARY_INSTALLED_MACHINES_DIR
#  
#
# global input
   global dbc_libref
   global tcl_platform

   set base_directory [MOM_ask_env_var UGII_CAM_LIBRARY_INSTALLED_MACHINES_DIR]
   if { $base_directory == "" } \
   {   # no chance to determine path info
       return ""
   }

   # needed later
   set separator "/"
   if { $tcl_platform(platform) == "windows" } \
   {
       set separator "\\"
   }

   # concatenate the actual machine directory name with the base directory name
   # the machine directory is based on the libref


   if { $db_file_name == "" } \
   {    # try with libref but without revision
        set actual_file_name ""
        append actual_file_name $dbc_libref ".dat"
   } \
   else \
   {    # try with the specified file name
 
        set actual_file_name $db_file_name

   }


   set test_success [test_file_in_directory $base_directory \
                                            $dbc_libref     \
                                            $separator      \
                                            $actual_file_name]


   set file_name_ret ""
   if { $test_success == 1 } \
   {
        append file_name_ret \
        "\$\{UGII_CAM_LIBRARY_INSTALLED_MACHINES_DIR\}"  $dbc_libref  $separator $actual_file_name
   }

   return $file_name_ret         
}




proc create_file_name_old { db_file_name } \
{
#
#  procedure creates a complete file name where the machine config .dat file
#  is located using the old MACH machine management structure
#  
#
# global input
   global dbc_libref


   set directory [MOM_ask_env_var UGII_CAM_LIBRARY_MACHINE_CONFIG_DIR]
   if { $directory == "" } \
   {   # no chance to determine path info
       return ""
   }


   set test_file_name ""
   if { $db_file_name == "" } \
   {
#      no file name in database, go with the libref
       set actual_file_name ""
       append actual_file_name $dbc_libref ".dat"

   } \
   else \
   {
#      take the file name as specified in the database
       set actual_file_name $db_file_name
   }

   set test_file_name ""
   append test_file_name $directory $actual_file_name

   set file_name_ret ""
   if { [file exists $test_file_name] } \
   {  
       # we return the version with the unresolved env variable
       append file_name_ret \
                "\$\{UGII_CAM_LIBRARY_MACHINE_CONFIG_DIR\}"  $actual_file_name
   }
 
   return $file_name_ret         
}


proc test_file_in_directory { base_dir actual_dir separator file } \
{   
   set test_file_name ""
   append test_file_name $base_dir $actual_dir $separator  $file

   if { [file exists $test_file_name] } \
   {
        return 1
   } 

   return 0
}



proc ASC_array_search_machine { libref db_row_ref } \
{
upvar $db_row_ref db_row

global asc_file_name

   set found [ASC_array_find_object_by_att LIBRF $libref db_row ]

   if { $found == 2 } \
   {
       set message "Error retrieving machine data from external library."
       set message "$message \n Machine with the library reference $libref"
       set message "$message \n does not exist in the"
       set message "$message \n ASCII Data File(s):"
       set message "$message \n $asc_file_name"
       MOM_log_message "\n $message"
   }

   return $found
}

proc DBC_ask_library_values {} \
{
#
# global input
   global dbc_libref
   global dbc_db_ids_count
   global dbc_db_ids

#
# global output
   global dbc_db_ids_value



#
# Look for the desired libref
#
    ASC_array_search_machine $dbc_libref db_row 

#
# and set the desired values
#
    for { set inx 0 } { $inx < $dbc_db_ids_count } { incr inx } \
    {
        set dbc_db_ids_value($dbc_db_ids($inx)) \
           [ASC_ask_att_val $dbc_db_ids($inx) $db_row "" "" flag]
    }
}








#---------------------------------------------
proc DBC_translate_att_alias {} {
#---------------------------------------------

   ASC_translate_att_alias
}

#---------------------------------------------
proc DBC_create_criterion {} {
#---------------------------------------------

    ASC_create_criterion
}

#---------------------------------------------
proc DBC_create_query {} {
#---------------------------------------------

    ASC_create_query
}

#---------------------------------------------
proc DBC_execute_query {} {
#---------------------------------------------

   ASC_execute_query
}
#---------------------------------------------
proc DBC_execute_query_for_count {} {
#---------------------------------------------

   ASC_execute_query_for_count

}

#------------------------------------------------------------------------
proc DBC_write { } {
#------------------------------------------------------------------------
global asc_database_name

    set asc_database_name "machine_database.dat"
    ASC_write
}

#---------------------------------------------------------------------
proc ASC_output_header { fileid } {
#---------------------------------------------------------------------
global dbc_attr_count
global dbc_attr_aliases
global dbc_attr_id
global dbc_logname
global asc_database_fmt


    set daytime [clock seconds]
    set out_daytime [clock format $daytime -format "%a %b %d %Y %I:%M %p"]

    set headerMessage "###############################################################################
# machine_database.dat - database file for machine database stored as ascii file
###############################################################################
#############################################################################
##  Data file for the Machine tool
##
##
##  REVISED:
##     $dbc_logname    $out_daytime
##
##  The following key words for Attribute ids are defined
##
##  LIBRF   - unique record identifier
##	T       - Machine type - MDM0101 -Mill machines
##                         - MDM0104 -TurnMill machines
##                         - MDM0201 -Lathe machines
##                         - MDM0204 -MillTurn machines
##                         - MDM0301 -Wedm machines
##                         - MDM0901 -Generic machines
##
##  MNF     - Manufacturer
##  DESCR   - Short description ( for example 3 Axis Mill) 
##  CNTR    - indicating the controller of the machine
##
##  POST    - the configuration file name with the list of
##            postprocessors for this machine 
##            (The path will be found from the search path environment variable)
##  RIGID   - Rigidity factor to apply to data from the machining data library
##            This will normally be a number less than or equal to 1.0
##  GRAPHICS- Geometry of the main machine tool prt file including full path
##            
##############################################################################"

    puts $fileid "$headerMessage"

#  load the database structure
    DBC_load_rset_data


#  output FORMAT line
#  As long as there is only one FORMAT line in the library file we
#    can use that one and skip rebuilding it
     set format_line "FORMAT"
     set format_line "$format_line $asc_database_fmt(0)"
#    for {set i 0} {$i < $dbc_attr_count} {incr i} \
#    {
#        DBC_ask_attr_id $dbc_attr_aliases($i)
#        set format_line "$format_line $dbc_attr_id"
#    }
    puts $fileid $format_line
    puts $fileid "#--------------------------------------------------------------------------------"
}
